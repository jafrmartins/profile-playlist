// Dependencies
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

// Routes
import LoginRoute from './components/route/LoginRoute';
import AppRoute from './components/route/AppRoute';

// Pages
import Login from './components/Login';
import Playlist from './components/Playlist';
import About from './components/About';
import PrivacyPolicy from './components/PrivacyPolicy';

// CSS
import './App.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/">
              <Redirect to="/playlist" />
            </Route>
            <LoginRoute path="/login" component={Login} />
            <AppRoute path="/playlist" component={Playlist} />
            <AppRoute path="/about" component={About} />
            <AppRoute isPublicRoute={true} path="/privacy-policy" component={PrivacyPolicy} />
            <Route path="*" render={props => (
              <h1>404 Page Not Found</h1>
            )} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
