import React from 'react'

const PlaylistThumbnail = (props) => {
  return (
    <img 
      src={`https://img.youtube.com/vi/${props.id}/1.jpg`} 
      onClick={() => { props.playbackItem.call(this, props.id, props.index) }} alt="thumbnail"/>
  )
}


export default PlaylistThumbnail;