// Dependencies
import React, { Component } from 'react';
import YouTube from 'react-youtube';

// Components
import PlaylistThumbnail from './PlaylistThumbnail';

// Services
import playlist from '../service/playlist';

export class Playlist extends Component {

  state = {
    currentItem: null,
    currentIndex: -1,
    options: {
      playerVars: {
        autoplay: 1,
        rel: 0,
        origin: 'localhost'
      }
    }
  }
  
  componentWillMount() {
    if(playlist.videos.length) {
      this.setState({
        currentItem: playlist.videos[0].id, currentIndex: 0
      })
    }
  }

  playbackItem = (videoId, index) => {
    this.setState({
      currentItem: videoId, currentIndex: index
    })
  }

  createPlaylist = () => {
    let playlistItems = null
    if(playlist.videos.length) {
      playlistItems = playlist.videos.map((video, index) => {
        return <li key={index}><PlaylistThumbnail id={video.id} index={index} playbackItem={this.playbackItem} /></li>
      })
    } return playlist.videos.length ? <ul>{playlistItems}</ul> : null;
  }

  getNextItem = (currentIndex) => {
    let i = 0;
    if ((currentIndex + 1) < playlist.videos.length) {
      i = currentIndex + 1
    } return { currentItem: playlist.videos[i].id, currentIndex: i }
  }

  onPlayerEnd = (e) => {
    const state = this.getNextItem(this.state.currentIndex)
    this.setState(state)
  }

  onPlayerError = (e) => {
    const state = this.getNextItem(this.state.currentIndex)
    this.setState(state)
  }

  createPlayer = () => {
    return playlist.videos.length ? (
      <YouTube
        id="youtube-player"
        className="player"
        opts={this.state.options}
        videoId={this.state.currentItem}
        onEnd={this.onPlayerEnd}
        onError={this.onPlayerError}
      />
    ) : null;
  }

  render() {
    return (
      <div className="player-container">
        { this.createPlayer() }
        { this.createPlaylist() }
      </div>
    )
  }
}

export default Playlist;
