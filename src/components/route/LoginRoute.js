// Dependencies
import React from 'react';
import { Route } from 'react-router-dom';

// Layout
import LoginLayout from '../layout/LoginLayout';

const LoginRoute = ({component: Component, ...rest}) => {
  return (
    <Route {...rest} render={props => (
      <LoginLayout>
          <Component {...props} />
      </LoginLayout>
    )} />
  )
};

export default LoginRoute;