// Dependencies
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

// Layout
import AppLayout from '../layout/AppLayout';

// Services
import auth from '../../service/auth';

const AppRoute = ({component: Component, isPublicRoute, ...rest}) => {
  return (
    <Route {...rest} render={props => 
      (auth.isAuthenticated === true || isPublicRoute === true) ? (
        <AppLayout>
          <Component {...rest} {...props} />
        </AppLayout>
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location },
          }}
        />
      )
    } />
  )
};

AppRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  component: PropTypes.func.isRequired,
  redirect: PropTypes.string,
}

export default AppRoute;
