// Dependencies
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';

// Services
import auth from '../service/auth';
import playlist from '../service/playlist';

class Login extends Component {
  
  state = {
    redirectToReferrer: false,
  }

  login = () => {
    this.setState(() => ({
      redirectToReferrer: true
    }))
  }

  processPosts = (posts) => {
    const videos = posts.data.filter((post) => {
      if (post.message && post.message.match(/youtube\.com/)) {
        return post.message.length;
      } return false;
    }).map((post) => {
      return post.message.split("&").shift();
    }); return videos;
  }

  responseFacebook = (response, callback) => {
    auth.login(response, (err) => {
      if (err) return false;
      const { posts } = response
      if (posts) {
        playlist.setVideos(this.processPosts(posts))
        this.login()
      } else {
        const url = `https://graph.facebook.com/v2.9/${response.userID}/posts?access_token=${response.accessToken}&limit=25`
        window.fetch(url)
          .then((res) => {
            return res.json()
          })
          .then((posts) => {
            playlist.setVideos(this.processPosts(posts))
            this.login()
          })
      }
    })
  }
  
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { redirectToReferrer } = this.state

    if (redirectToReferrer === true) {
      return <Redirect to={from} />
    }

    return (
      <FacebookLogin
        appId={process.env.REACT_APP_FACEBOOK_APP_ID}
        autoLoad={true}
        fields={process.env.REACT_APP_FACEBOOK_APP_FIELDS}
        scope={process.env.REACT_APP_FACEBOOK_APP_SCOPE}
        callback={this.responseFacebook} 
        render={renderProps => (
          <button>Login With Facebook</button>
        )}/>
    )
  }

}

export default Login;
