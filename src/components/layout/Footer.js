import React from 'react';

const Footer = (props) => {
  return (
    <footer>
      <div><span>{process.env.REACT_APP_AUTHOR}</span> | <span>{process.env.REACT_APP_TITLE}</span> &copy; 2019</div>
    </footer>
  )
}

export default Footer;