// Dependencies
import React from 'react';

// Components
import Header from './Header';
import Footer from './Footer';

const AppLayout = ({children, ...rest}) => {
  return (
    <div className="page page-app">
      <Header />
      <div className="container main">
        {children}
      </div>
      <Footer />
    </div>
  )
}

export default AppLayout;
