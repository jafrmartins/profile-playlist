// Dependencies
import React from 'react';
import { Link } from 'react-router-dom';

// Components
import Navigation from './Navigation';

const Header = (props) => {

  return (
    <header>
      <Link to="/">{process.env.REACT_APP_TITLE}</Link>
      <Navigation/>
    </header>
  )
  
}

export default Header;