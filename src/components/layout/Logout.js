// Dependencies
import React from 'react';
import { withRouter } from 'react-router-dom';

// Services
import auth from '../../service/auth';

const Logout = withRouter(({ history }) => (
  <button onClick={() => {
    auth.logout(() => history.push('/'))
  }}>Logout</button>
))

export default Logout;