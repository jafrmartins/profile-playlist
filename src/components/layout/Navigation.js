// Dependencies
import React, { Component } from 'react'
import { Link } from 'react-router-dom';

// Services
import auth from '../../service/auth';

// Components
import Logout from './Logout';

export class Navigation extends Component {

  renderNavItems = () => {
    
    let items = [];
    if(auth.isAuthenticated) {
      items = [
        <li key={0}><Link to="/playlist">Playlist</Link></li>,
        <li key={1}><Link to="/about">About</Link></li>,
      ]
    } 
    items = [...items, <li key={items.length}><Link to="/privacy-policy">Privacy Policy</Link></li>]
    if(auth.isAuthenticated) {
      items = [...items, <li key={items.length}><Logout /></li>]
    } 
    return items;
          
  }

  renderNavUserPicture = () => {
    return auth.isAuthenticated ? (
      <span className="userThumbnail"><img src={auth.picture} alt="user thumbnail"/></span>
    ) : null; 
  }

  render() {
    return (
      <nav>
        {this.renderNavUserPicture()}
        <ul>
          {this.renderNavItems()}
        </ul>
      </nav>
    )
  }
}

export default Navigation;
