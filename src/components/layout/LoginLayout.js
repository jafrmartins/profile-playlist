// Dependencies
import React from 'react';

// Components
import Header from './Header';
import Footer from './Footer';

const LoginLayout = ({children, ...rest}) => {
  return (
    <div className="page page-login">
      <Header />
      <div className="container main">
        {children}
      </div>
      <Footer />
    </div>
  )
}

export default LoginLayout;
