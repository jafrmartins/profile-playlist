class Playlist {

  constructor() {
    this.videos = [];
  }

  setVideos(videos) {
    this.videos = videos.map((url) => {
      const id = url.split("v=").pop()
      return { id }
    });
  } 

}

export default new Playlist();