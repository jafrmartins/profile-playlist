class Auth {

  constructor() {
    this.isAuthenticated = false;
    this.email = null;
    this.userID = null;
    this.accessToken = null;
    this.picture = null;
  }

  login(response, next) {
    console.log(response)
    this.isAuthenticated = true;
    const { email, picture, userID, accessToken } = response
    if (accessToken) {
      this.email = email;
      this.userID = userID;
      this.accessToken = accessToken;
      if (response.picture) {
        this.picture = picture.data.url
      }
      return next();
    }
    next(true);
  }

  logout(next) {
    this.isAuthenticated = false;
    this.email = null;
    this.userID = null;
    this.accessToken = null;
    this.picture = null;
    next()
  }

}

export default new Auth();